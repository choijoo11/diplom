import React, { Component } from 'react';
import { Image,View } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
export default class Index extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='home' />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right />
        </Header>
        <Content>
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Image 
          resizeMode="cover" 
          source={require('./home.jpg')} 
          style={{width: 300,height:200}}
        />
         </View>
        </Content>
       
      </Container>
    );
  }
}