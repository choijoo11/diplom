import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import lesson from './lesson';
import faq from './faq';
const MainNavigator = createStackNavigator(
    {lesson,faq},
    { initialRouteName: 'faq'}
);

const App = createAppContainer(MainNavigator);

export default App;