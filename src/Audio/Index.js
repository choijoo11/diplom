import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import audio from './audio';

const MainNavigator = createStackNavigator(
    {audio},
    { initialRouteName: 'audio'}
);

const App = createAppContainer(MainNavigator);

export default App;