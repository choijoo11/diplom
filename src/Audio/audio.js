import React, { useState, useEffect } from 'react';
import { StyleSheet, Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Left, Body } from 'native-base';
import axios from 'axios';

const audio = () => {
  const [item, setItem] = useState({});

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    axios.post('https://choi.zoloo.site/api/v1/category/1')
      .then(function (response) {
        // handle success
        if (response.data.success === 1) {
          if (response.data.result.length > 0) {
            setItem(response.data.result);
            console.log(response.data.result);
          }
        }
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
  }

  const render = () => {
    return Object.keys(item).map((data, index) => {
      return <Card style={{ flex: 0 }} key={index}>
        <CardItem>
          <Left>
            <Thumbnail source={{ uri: `${item[data].image}` }} />
            <Body>
              <Text>{item[data].name}</Text>
              <Text note>{item[data].created_at.substring(0, 10)}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem>
          <Body>
            <Image source={{ uri: `${item[data].image}` }} style={{ height: 200, width: '100%', flex: 1 }} />
            <Text>
              {item[data].text}
            </Text>
          </Body>
        </CardItem>
      </Card>
    })
  }
  return (
    <Container>
      <Header />
      <Content>
        {render()}
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  blue: {
    flex: 1
  },
  container: {
    backgroundColor: 'red'
  },
  renderName: {
    flex: 1,
    backgroundColor: 'red'
  },
  z: {
    flex: 1,
    height: 100,
    borderWidth: 2,
    borderColor: 'green',
  },
  textz: {
    color: '#fff',
    fontSize: 22,
  }
})

audio.navigationOptions = screenProps => ({
  header: null
});
export default audio;