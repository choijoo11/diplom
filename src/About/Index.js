import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import About from './About';
import Faq from './Faq';
const MainNavigator = createStackNavigator(
    {About,Faq},
    { initialRouteName: 'Faq'}
);

const App = createAppContainer(MainNavigator);

export default App;
