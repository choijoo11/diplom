import React from 'react';
import { Text, View,Button } from 'react-native';


class About extends React.Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        
   
        <Text>About me!</Text>
        <Button
        title="about Page"
        onPress={() => navigate('Faq')}
      />
      </View>
    );
  }
}

export default About;