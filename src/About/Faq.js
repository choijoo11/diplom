import React from 'react';
import { Text, View,Button ,Image,StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  b: {
    flex:1
},
backgroundImage: {
  flex: 1,
  resizeMode: 'contain'
},})

class About extends React.Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View style={StyleSheet.container}/>
      <Image source={require('./feedbak.png')} style={styles.backgroundImage}/>
   
        <Text>click!</Text>
        <Button
        title="Go to About Page"
        onPress={() => navigate('About')}
      />
      </View>
    );
  }
}

export default About;