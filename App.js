import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Icon } from 'native-base'
import HomeScreen from './src/Home/Index'
import LessonScreen from './src/Lesson/Index'
import AboutScreen from './src/About/Index'
import AudioScreen from './src/Audio/Index'

const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarLabel: "home",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="home" size={30} color="#900" />
      )
    }
  },
  Lesson: {
    screen: LessonScreen,
    navigationOptions: {
      tabBarLabel: "lesson",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="book" size={30} color="#900" />
      )
    }
  },
  Audio: {
    screen: AudioScreen,
    navigationOptions: {
      header: null,
      tabBarLabel: "audio",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="ios-headset" size={30} color="#900" />
      )
    }
  },
  feedback: {
    screen: AboutScreen,
    navigationOptions: {
      tabBarLabel: "about",
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-settings" size={30} color="#900" />
      )
    }
  },
},
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: 'red',
      inactiveTintColor: 'black',
      style: {
        backgroundColor: '#3d3c85',

      },
    },

  });

export default createAppContainer(TabNavigator);